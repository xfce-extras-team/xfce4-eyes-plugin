xfce4-eyes-plugin (4.6.2-1) unstable; urgency=medium

  * New upstream version 4.6.2.
  * d/control: Replace intltool with gettext.
  * d/copyright: Update years.
  * d/rules:
    - Use execute_after instead of overriding dh_auto_install.
    - Opt-out of trimmed changelogs, install NEWS as changelog.
  * Update Standards-Version to 4.7.1.

 -- Unit 193 <unit193@debian.org>  Tue, 04 Mar 2025 00:30:21 -0500

xfce4-eyes-plugin (4.6.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit (from ./configure).
  * Update standards version to 4.5.1, no changes needed.
  * Avoid explicitly specifying -Wl,--as-needed linker flag.

  [ Unit 193 ]
  * New upstream version 4.6.0.
  * d/copyright: Update years.
  * Update Standards-Version to 4.6.0.

 -- Unit 193 <unit193@debian.org>  Wed, 20 Apr 2022 04:16:09 -0400

xfce4-eyes-plugin (4.5.1-1) unstable; urgency=medium

  * d/watch: Update for upstream's move to mirrorbit.
  * New upstream version 4.5.1.
  * d/compat, d/control:
    - Drop d/compat in favor of debhelper-compat, bump to 13.
  * d/control, d/copyright: Update my email address, bump years.
  * d/control:
    - Update homepage.
    - Set xfce4-eyes-plugin M-A: same.
    - R³: no.
  * d/rules: Drop dh_autoreconf and dh_clean overrides.
  * d/watch: Use uscan special strings.
  * Update Standards-Version to 4.5.0.

 -- Unit 193 <unit193@debian.org>  Wed, 24 Jun 2020 20:51:22 -0400

xfce4-eyes-plugin (4.5.0-2) unstable; urgency=low

  * d/control:
    - Update my (James') email.
    - Move Vcs-* links to salsa.debian.org
    - Bump Standards-Version to 4.1.4, no changes needed.
  * Switch Homepage, d/watch links to https://

 -- James Lu <james@overdrivenetworks.com>  Tue, 05 Jun 2018 19:42:51 -0700

xfce4-eyes-plugin (4.5.0-1) unstable; urgency=medium

  [ Unit 193 / James Lu ]
  * Initial release (Closes: #407380)

 -- Unit 193 <unit193@ubuntu.com>  Mon, 24 Jul 2017 04:19:22 -0400
