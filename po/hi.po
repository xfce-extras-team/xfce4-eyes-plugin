# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Xfce
# This file is distributed under the same license as the xfce-panel-plugins.xfce4-eyes-plugin package.
# 
# Translators:
# Panwar108 <caspian7pena@gmail.com>, 2019
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/\n"
"POT-Creation-Date: 2024-05-07 12:59+0200\n"
"PO-Revision-Date: 2013-07-03 18:59+0000\n"
"Last-Translator: Panwar108 <caspian7pena@gmail.com>, 2019\n"
"Language-Team: Hindi (http://app.transifex.com/xfce/xfce-panel-plugins/language/hi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hi\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: panel-plugin/eyes.c:365 panel-plugin/eyes.desktop.in:5
msgid "Eyes"
msgstr "आँखें"

#: panel-plugin/eyes.c:368
msgid "_Close"
msgstr "बंद करें (_C)"

#: panel-plugin/eyes.c:385
msgid "_Select a theme:"
msgstr "थीम चुनें (_S) :"

#: panel-plugin/eyes.c:421
msgid "Use single _row in multi-row panel"
msgstr "बहु-पंक्ति पैनल में एकल पंक्ति का उपयोग करें (_r)"

#: panel-plugin/eyes.desktop.in:6
msgid "Eyes that spy on you"
msgstr "आपकी जासूसी हेतु आँखें"
